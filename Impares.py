uno = int(input("Dame un número entero no negativo: "))
dos = int(input("Dame otro: "))
if uno < dos:
    inicio = uno
    final = dos
else:
    inicio = dos
    final = uno
suma = 0
if inicio % 2 == 0:
    if final % 2 == 0:
        for x in range (inicio+1, final, 2):
            suma = x + suma
    elif final % 2 != 0:
        for x in range (inicio+1, final+1, 2):
            suma = x + suma
elif inicio % 2 != 0:
    if final % 2 == 0:
        for x in range(inicio, final, 2):
            suma = x + suma
    elif final % 2 != 0:
        for x in range(inicio, final+1, 2):
            suma = x + suma
print("Suma de los números impares entre", inicio, "y", final, "es", suma)